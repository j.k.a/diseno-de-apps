import React, { useEffect, useState } from "react";
import { View, Text, TextInput, Button, StyleSheet } from "react-native";
import { DataTable } from "react-native-paper";
import axios from "axios";

const MyComponent = () => {
  const [animalName, setAnimalName] = useState("");
  const [animalData, setAnimalData] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        "https://api.api-ninjas.com/v1/animals",
        {
          params: {
            name: animalName,
          },
          headers: {
            "X-Api-Key": "jKxzt7Gy/5MFsvboMRrhPw==c2mdD1pNRDA9j3o3",
          },
        }
      );
      setAnimalData(response.data);
    } catch (error) {
      console.error("Error: ", error);
    }
  };

  const handleSearch = () => {
    fetchData();
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Ingresa el nombre del animal"
        value={animalName}
        onChangeText={setAnimalName}
      />
      <Button title="Buscar" onPress={handleSearch} />

      <DataTable>
        <DataTable.Header>
          <DataTable.Title style={styles.header}>Nombre</DataTable.Title>
          <DataTable.Title style={styles.header}>Reino</DataTable.Title>
          <DataTable.Title style={styles.header}>Clase</DataTable.Title>
          <DataTable.Title style={styles.header}>Orden</DataTable.Title>
          <DataTable.Title style={styles.header}>Familia</DataTable.Title>
          <DataTable.Title style={styles.header}>
            Nombre Científico
          </DataTable.Title>
        </DataTable.Header>

        {animalData.map((animal, index) => (
          <DataTable.Row key={index}>
            <DataTable.Cell style={styles.cell}>{animal.name}</DataTable.Cell>
            <DataTable.Cell style={styles.cell}>
              {animal.taxonomy.kingdom}
            </DataTable.Cell>
            <DataTable.Cell style={styles.cell}>
              {animal.taxonomy.class}
            </DataTable.Cell>
            <DataTable.Cell style={styles.cell}>
              {animal.taxonomy.order}
            </DataTable.Cell>
            <DataTable.Cell style={styles.cell}>
              {animal.taxonomy.family}
            </DataTable.Cell>
            <DataTable.Cell style={styles.cell}>
              {animal.taxonomy.scientific_name}
            </DataTable.Cell>
          </DataTable.Row>
        ))}
      </DataTable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
    paddingHorizontal: 20,
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  header: {
    fontWeight: "bold",
    fontSize: 18,
    textAlign: "center",
  },
  cell: {
    fontSize: 16,
    marginBottom: 5,
    textAlign: "center",
  },
});

export default MyComponent;
