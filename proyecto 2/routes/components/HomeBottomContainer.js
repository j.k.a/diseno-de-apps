import React, { useMemo } from "react";
import { Image } from "expo-image";
import { StyleSheet, Text, View, ImageSourcePropType } from "react-native";
import { Padding, Color, FontFamily, FontSize } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const HomeBottomContainer = ({
  dimensionsCode,
  propOverflow,
  propMarginTop,
  propHeight,
  propMarginLeft,
}) => {
  const bottomNavStyle = useMemo(() => {
    return {
      ...getStyleValue("overflow", propOverflow),
      ...getStyleValue("marginTop", propMarginTop),
    };
  }, [propOverflow, propMarginTop]);

  const tabStyle = useMemo(() => {
    return {
      ...getStyleValue("height", propHeight),
      ...getStyleValue("marginLeft", propMarginLeft),
    };
  }, [propHeight, propMarginLeft]);

  return (
    <View style={[styles.bottomNav, styles.bottomNav]}>
    <View style={[styles.tab, styles.tabFlexBox]}>
            <Image
              style={styles.image6Icon}
              contentFit="cover"
              source={require('../assets/casa.png')}
            />
          <Text style={[styles.title, styles.iconFlexBox]} numberOfLines={1}>
            Inicio
          </Text>
        </View>
  </View>
  );
};

const styles = StyleSheet.create({
  tabFlexBox: {
    padding: Padding.p_9xs,
    alignItems: "center",
    flex: 1,
  },
  iconFlexBox: {
    display: "flex",
    textAlign: "center",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  image6Icon: {
    width: 21,
    height: 21,
  },
  title: {
    alignSelf: "stretch",
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    height: 14,
  },
  tab: {
    justifyContent: "center",
    padding: Padding.p_9xs,
    alignItems: "center",
    flex: 1,
    height: 53,
  },
  icon: {
    fontSize: FontSize.size_xl,
    lineHeight: 28,
    width: 28,
    height: 28,
  },
  tab1: {
    padding: Padding.p_9xs,
    alignItems: "center",
    flex: 1,
  },
  bottomNav: {
    width: 359,
    paddingHorizontal: 123,
    paddingVertical: 0,
    height: 53,
    overflow: "hidden",
    shadowOpacity: 1,
    elevation: 6,
    shadowRadius: 6,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: "rgba(0, 0, 0, 0.12)",
    backgroundColor: Color.colorWhite,
  },
});

export default HomeBottomContainer;
