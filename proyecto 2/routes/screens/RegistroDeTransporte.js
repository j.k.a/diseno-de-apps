import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import PasswordFormButtonContainer from "../components/PasswordFormButtonContainer";
import { Padding, Color, FontFamily, FontSize, Border } from "../GlobalStyles";

const RegistroDeTransporte = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };
  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.registroDeTransporte}>
        <View style={[styles.content, styles.contentFlexBox]}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Registro de transporte</Text>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Matricula</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca la matricula
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Descripcion</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text1, styles.textTypo]} numberOfLines={6}>
              Introduzca el numero del transporte y cantidad de asientos
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Marca</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca la marca
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Modelo</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca el modelo
            </Text>
          </View>
        </View>
        <PasswordFormButtonContainer
          resetPasswordBtnText="Cancelar"
          actionButtonText="Registrar"
          propHeight="unset"
          propHeight1="unset"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  inputSpaceBlock: {
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  textTypo: {
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    overflow: "hidden",
    flex: 1,
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    lineHeight: 24,
    marginLeft: 8,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    alignSelf: "stretch",
  },
  text: {
    height: 20,
  },
  textfield: {
    borderRadius: Border.br_7xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    marginTop: 4,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    justifyContent: "center",
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
  },
  text1: {
    height: 120,
  },
  registroDeTransporte: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default RegistroDeTransporte;
