import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import PropertyListContainer from "../components/PropertyListContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { Color, FontFamily, Border, Padding, FontSize } from "../GlobalStyles";

const Rutas = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };
  const CentroMat = () => {
    navigation.navigate("ZonaCentroMatutino");
  };
  const CentroVesp = () => {
    navigation.navigate("ZonaCentroVespertino");
  };
  const CentroNoct = () => {
    navigation.navigate("ZonaCentroNocturno");
  };
  const NatMat = () => {
    navigation.navigate("NaturaMatutino");
  };
  const NatVesp = () => {
    navigation.navigate("NaturaVespertino");
  };
  const NatNoct = () => {
    navigation.navigate("NaturaNocturno");
  };
  const VfMat = () => {
    navigation.navigate("VillaFontanaMatutino");
  };
  const VfVesp = () => {
    navigation.navigate("VillaFontanaVespertino");
  };
  const VfNoct = () => {
    navigation.navigate("VillaFontanaNocturno");
  };
  return (
    <ScrollView style={styles.safeArea}>
      <View style={styles.rutas}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.rowFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={require("../assets/icleft.png")}
              />
            </TouchableOpacity>
            <Text style={[styles.title, styles.titleTypo]}>Rutas</Text>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.rowFlexBox]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>Ruta 1</Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={styles.rowFlexBox}>
            <View style={styles.cardLayout}>
              <View style={styles.textContent}>
                <Text style={[styles.subtitle, styles.titleTypo]}>
                  Zona Centro
                </Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <Text style={[styles.title2, styles.text1Typo]}>
                  Map of Route 1
                </Text>
                <Image
                  style={[styles.image1Icon, styles.imageLayout]}
                  contentFit="cover"
                  source={require("../assets/image-1.png")}
                />
              </View>
              <View style={styles.imageContainer} />
            </View>
            <View style={[styles.card1, styles.cardLayout]}>
              <View style={styles.textContent}>
                <Text style={[styles.subtitle, styles.titleTypo]}>
                  Horarios
                </Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <TouchableOpacity onPress={CentroMat}>
                  <View style={[styles.chip, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Matutino
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={CentroNoct}>
                  <View style={[styles.chip1, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Nocturno
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={CentroVesp}>
                  <View style={[styles.chip2, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Vespertino
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.rowFlexBox]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>Ruta 2</Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={styles.rowFlexBox}>
            <View style={styles.cardLayout}>
              <View style={styles.textContent}>
                <Text style={[styles.subtitle, styles.titleTypo]}>Natura</Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <Text style={[styles.title2, styles.text1Typo]}>
                  Map of Route 1
                </Text>
                <Image
                  style={[styles.image1Icon, styles.imageLayout]}
                  contentFit="cover"
                  source={require("../assets/image-2.png")}
                />
              </View>
              <View style={styles.imageContainer} />
            </View>
            <View style={[styles.card1, styles.cardLayout]}>
              <View style={styles.textContent}>
                <Text style={[styles.subtitle, styles.titleTypo]}>
                  Horarios
                </Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <TouchableOpacity onPress={NatMat}>
                  <View style={[styles.chip, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Matutino
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={NatNoct}>
                  <View style={[styles.chip1, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Nocturno
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={NatVesp}>
                  <View style={[styles.chip2, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Vespertino
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.rowFlexBox]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>Ruta 3</Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={styles.rowFlexBox}>
            <View style={styles.cardLayout}>
              <View style={styles.textContent}>
                <Text style={[styles.subtitle, styles.titleTypo]}>
                  Villa Fontana
                </Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <Text style={[styles.title2, styles.text1Typo]}>
                  Map of Route 1
                </Text>
                <Image
                  style={[styles.image1Icon, styles.imageLayout]}
                  contentFit="cover"
                  source={require("../assets/image-3.png")}
                />
              </View>
              <View style={styles.imageContainer} />
            </View>
            <View style={[styles.card1, styles.cardLayout]}>
              <View style={styles.textContent}>
                <Text style={[styles.subtitle, styles.titleTypo]}>
                  Horarios
                </Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <TouchableOpacity onPress={VfMat}>
                  <View style={[styles.chip, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Matutino
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={VfNoct}>
                  <View style={[styles.chip1, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Nocturno
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={VfVesp}>
                  <View style={[styles.chip2, styles.chipLayout]}>
                    <Text style={[styles.text1, styles.text1Typo]}>
                      Vespertino
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  titleTypo: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  listFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  imageLayout: {
    height: 170,
    width: 148,
  },
  text1Typo: {
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
  },
  cardLayout: {
    height: 210,
    width: 150,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderStyle: "solid",
    borderRadius: Border.br_7xs,
    overflow: "hidden",
    alignItems: "center",
  },
  chipLayout: {
    width: 85,
    left: 35,
    position: "absolute",
    backgroundColor: Color.colorGray_400,
    padding: Padding.p_5xs,
    borderRadius: Border.br_7xs,
    justifyContent: "center",
    alignItems: "center",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    fontSize: FontSize.size_lg,
    paddingHorizontal: Padding.p_3xs,
    textAlign: "left",
    alignSelf: "flex-start",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    marginTop: 12,
    alignItems: "center",
  },
  subtitle: {
    fontSize: FontSize.size_base,
    textAlign: "left",
    alignSelf: "stretch",
  },
  textContent: {
    padding: Padding.p_5xs,
    alignSelf: "stretch",
  },
  title2: {
    marginTop: -8,
    top: "50%",
    left: 16,
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    textAlign: "center",
    display: "flex",
    width: 116,
    height: 16,
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
  },
  image1Icon: {
    top: 0,
    left: 0,
    position: "absolute",
  },
  image: {
    backgroundColor: Color.colorGray_400,
    height: 170,
    width: 148,
  },
  imageContainer: {
    height: 150,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  text1: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    textAlign: "left",
  },
  chip: {
    top: 23,
  },
  chip1: {
    top: 111,
  },
  chip2: {
    top: 67,
  },
  card1: {
    marginLeft: 8,
  },
  list: {
    width: 335,
    paddingLeft: Padding.p_xs,
    paddingRight: Padding.p_base,
    marginTop: 12,
    overflow: "hidden",
  },
  rutas: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
});

export default Rutas;
