import * as React from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import DarkModeToggleContainer from "../components/DarkModeToggleContainer";
import ProfilePhotoUploaderContainer from "../components/ProfilePhotoUploaderContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { Padding, FontFamily, Color, Border, FontSize } from "../GlobalStyles";

const Ajustes = () => {
  const navigation = useNavigation();
  const irAProx = () => {
    navigation.navigate("Proximamente");
  };
  const irASoporte = () => {
    navigation.navigate("SoporteTecnico");
  };
  const irAtras = () => {
    navigation.goBack();
  };
  const irAcerca = () => {
    navigation.navigate("AcercaDeNosotros");
  };
  return (
    <ScrollView style={styles.container}>
      <View style={styles.Ajustes}>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Configuración</Text>
        </View>
        <View style={styles.tabGroup}>
          <TouchableOpacity onPress={irAProx} style={styles.tabBorder}>
            <Text style={styles.title1}>Modo oscuro</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={irAProx}
            style={[styles.tabBorder, styles.tab1]}
          >
            <Text style={styles.title1}>Notificaciones</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={irAProx}
            style={[styles.tabBorder, styles.tab1]}
          >
            <Text style={styles.title1}>Language</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.sectionTitle}>
          <Text style={styles.title4}>General</Text>
        </View>
        <View style={styles.list}>
          <TouchableOpacity onPress={irAProx}>
            <DarkModeToggleContainer
              iconLabel="🌑"
              featureLabel="Modo Oscuro"
              notificationSettingsLabel="Cambiar a modo oscuro"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irAProx}>
            <DarkModeToggleContainer
              iconLabel="🔔"
              featureLabel="Notificaciones"
              notificationSettingsLabel="Administra la configuración de notificaciones"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irAProx}>
            <DarkModeToggleContainer
              iconLabel="🌐"
              featureLabel="Language"
              notificationSettingsLabel="Switch to English"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.sectionTitle}>
          <Text style={styles.title4}>Cuenta</Text>
        </View>
        <TouchableOpacity onPress={irAProx}>
          <ProfilePhotoUploaderContainer
            iconOrText="📷"
            profileImageText="Foto de perfil"
            profileActionText="Cambiar o subir una foto de perfil"
          />
        </TouchableOpacity>
        <View style={styles.sectionTitle}>
          <Text style={styles.title4}>Soporte</Text>
        </View>
        <View style={styles.list}>
          <TouchableOpacity onPress={irASoporte} style={styles.item}>
            <Image
              style={styles.frameIcon}
              source={require("../assets/frame1.png")}
            />
            <View style={styles.titleParent}>
              <Text style={styles.title7}>Soporte Técnico</Text>
              <Text style={styles.subtitle}>Obtén asistencia técnica</Text>
            </View>
            <Image
              style={styles.itemChild}
              source={require("../assets/vector-2001.png")}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.sectionTitle}>
          <Text style={styles.title4}>Acerca de</Text>
        </View>
        <TouchableOpacity onPress={irAcerca}>
          <ProfilePhotoUploaderContainer
            iconOrText="📔"
            profileImageText="Acerca de nosotros"
            profileActionText="Conozca más sobre nuestra compañía"
          />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  topBar: {
    flexDirection: "row",
    alignItems: "center",
    padding: Padding.p_5xs,
    backgroundColor: Color.colorWhite,
    elevation: 6,
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 1,
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    flex: 1,
    marginLeft: 8,
    fontSize: FontSize.size_xl,
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
  },
  tabGroup: {
    flexDirection: "row",
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
  tabBorder: {
    flex: 1,
    padding: Padding.p_5xs,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderRadius: Border.br_7xs,
    alignItems: "center",
  },
  tab1: {
    marginLeft: 8,
  },
  Ajustes: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
  title1: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
  },
  sectionTitle: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
  title4: {
    fontSize: FontSize.size_lg,
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
  },
  list: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
  frameIcon: {
    width: 32,
    height: 32,
    borderRadius: Border.br_base,
  },
  item: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
  },
  titleParent: {
    flex: 1,
    marginLeft: 8,
  },
  title7: {
    fontSize: FontSize.size_sm,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorGray_500,
  },
  itemChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    zIndex: 2,
  },
});

export default Ajustes;
