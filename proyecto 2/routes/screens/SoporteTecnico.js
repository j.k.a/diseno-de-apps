import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  Image,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import ErrorReportingContainer from "../components/ErrorReportingContainer";
import Whatsapp from "../components/Whatsapp";
import { Linking } from "react-native";
import { FontFamily, Padding, FontSize, Color, Border } from "../GlobalStyles";

const SoporteTecnico = () => {
  const [email, setEmail] = useState("");
  const [telefono, setTelefono] = useState("");

  const formatTelefono = (input) => {
    // Eliminar caracteres no numéricos
    let cleaned = ("" + input).replace(/\D/g, "");

    // Formatear el número telefónico
    let formatted = "";
    if (cleaned.length > 0) {
      formatted += "(" + cleaned.substring(0, 3);
    }
    if (cleaned.length >= 3) {
      formatted += ") ";
      if (cleaned.length === 3 && input[input.length - 1] !== ")") {
        formatted += cleaned.substring(3);
      } else {
        formatted += cleaned.substring(3, 6);
      }
    }
    if (cleaned.length > 6) {
      formatted += "-" + cleaned.substring(6, 10);
    }
    return formatted;
  };

  const handleChangeTelefono = (input) => {
    // Formatear el número telefónico y actualizar el estado
    setTelefono(formatTelefono(input));
  };

  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };
  const irErrores = () => {
    navigation.navigate("ReporteDeErrores");
  };
  const irAWhatsapp = () => {
    Linking.openURL("https://wa.me/qr/HH2324AHZZJOP1");
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.soporteTecnico}>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={[styles.title, styles.titleTypo]}> Soporte técnico</Text>
        </View>
        <View style={styles.sectionTitle}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Información de contacto
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title2, styles.text1Typo]}>Email</Text>
          <View style={[styles.textfield, styles.textfieldBorder]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={email}
              onChangeText={setEmail}
              placeholder="correo@example.com"
              keyboardType="email-address"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title2, styles.text1Typo]}>Teléfono</Text>
          <View style={[styles.textfield, styles.textfieldBorder]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={telefono}
              onChangeText={handleChangeTelefono}
              placeholder="(###) ### - ####"
              keyboardType="phone-pad" // Teclado específico para números telefónicos
            />
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.itemFlexBox]}>
          <View style={styles.text}>
            <Text style={styles.title}>Reportar errores</Text>
          </View>
        </View>
        <TouchableOpacity onPress={irErrores}>
          <ErrorReportingContainer />
        </TouchableOpacity>
        <TouchableOpacity onPress={irAWhatsapp}>
          <Whatsapp />
        </TouchableOpacity>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title2, styles.text1Typo]}>Opinion</Text>
          <View style={[styles.textfield, styles.textfieldBorder]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              placeholder="Escribe tu opinión aquí"
              minHeight={150} // Altura mínima del cuadro de texto
            />
          </View>
        </View>
        <View style={[styles.button, styles.inputSpaceBlock]}>
          <View style={styles.primary}>
            <Text style={[styles.title6, styles.titleTypo]}>
              Enviar opinión
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  topBar: {
    flexDirection: "row",
    alignItems: "center",
    padding: Padding.p_5xs,
    backgroundColor: Color.colorWhite,
    elevation: 6,
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 1,
  },
  itemFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
  },
  inputSpaceBlock: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  text1Typo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  textfieldBorder: {
    marginTop: 4,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderStyle: "solid",
    borderRadius: Border.br_7xs,
    alignSelf: "stretch",
    alignItems: "center",
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    textAlign: "left",
    color: Color.colorBlack,
    alignSelf: "stretch",
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  title1: {
    fontSize: FontSize.size_lg,
    color: Color.colorBlack,
    lineHeight: 24,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  title2: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  text1: {
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorGray_500,
    height: 20,
    overflow: "hidden",
    flex: 1,
  },
  textfield: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
  },
  input: {
    justifyContent: "center",
  },
  title6: {
    fontSize: FontSize.size_base,
    lineHeight: 22,
    color: Color.colorWhite,
  },
  primary: {
    borderRadius: Border.br_5xs,
    backgroundColor: Color.colorBlack,
    paddingVertical: Padding.p_3xs,
    justifyContent: "center",
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    flex: 1,
  },
  button: {
    flexDirection: "row",
  },
  image: {
    backgroundColor: Color.colorGray_300,
    height: 163,
    borderRadius: Border.br_7xs,
    marginTop: 12,
    alignSelf: "stretch",
  },
  soporteTecnico: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default SoporteTecnico;
