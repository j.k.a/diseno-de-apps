import("node-fetch")
  .then((fetch) => {})
  .catch((error) => {
    console.error("Error al importar node-fetch:", error);
  });

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

async function obtenerListaPendientes() {
  try {
    const response = await fetch("http://jsonplaceholder.typicode.com/todos");
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error al obtener la lista de pendientes:", error);
    return [];
  }
}

function imprimirPendientesPorIDs(pendientes) {
  const ids = pendientes.map((pendiente) => pendiente.id);
  console.log("Lista de todos los pendientes (solo IDs):", ids);
}

function imprimirPendientesPorIDsYTitulos(pendientes) {
  const idTitulo = pendientes.map((pendiente) => ({
    id: pendiente.id,
    title: pendiente.title,
  }));
  console.log("Lista de todos los pendientes (IDs y Titles):", idTitulo);
}

function imprimirPendientesNoResueltosPorIDsYTitulos(pendientes) {
  const noResueltos = pendientes.filter((pendiente) => !pendiente.completed);
  console.log(
    "Lista de todos los pendientes sin resolver (ID y Title):",
    noResueltos
  );
}

function imprimirPendientesResueltosPorIDsYTitulos(pendientes) {
  const resueltos = pendientes.filter((pendiente) => pendiente.completed);
  console.log(
    "Lista de todos los pendientes resueltos (ID y Title):",
    resueltos
  );
}

function imprimirPendientesPorIDsYUserID(pendientes) {
  const idUserID = pendientes.map((pendiente) => ({
    id: pendiente.id,
    userId: pendiente.userId,
  }));
  console.log("Lista de todos los pendientes (IDs y userID):", idUserID);
}

function imprimirPendientesResueltosPorIDsYUserID(pendientes) {
  const resueltos = pendientes
    .filter((pendiente) => pendiente.completed)
    .map((pendiente) => ({ id: pendiente.id, userId: pendiente.userId }));
  console.log(
    "Lista de todos los pendientes resueltos (ID y userID):",
    resueltos
  );
}

function imprimirPendientesNoResueltosPorIDsYUserID(pendientes) {
  const noResueltos = pendientes
    .filter((pendiente) => !pendiente.completed)
    .map((pendiente) => ({ id: pendiente.id, userId: pendiente.userId }));
  console.log(
    "Lista de todos los pendientes sin resolver (ID y userID):",
    noResueltos
  );
}

function mostrarMenu() {
  console.log("1. Lista de todos los pendientes (solo IDs)");
  console.log("2. Lista de todos los pendientes (IDs y Titles)");
  console.log("3. Lista de todos los pendientes y sin resolver (ID y Title)");
  console.log("4. Lista de todos los pendientes resueltos (ID y Title)");
  console.log("5. Lista de todos los pendientes (IDs y userID)");
  console.log("6. Lista de todos los pendientes resueltos (ID y userID)");
  console.log("7. Lista de todos los pendientes sin resolver (ID y userID)");
  console.log("8. Salir");

  rl.question("Seleccione una opción del 1 al 8: ", (opcion) => {
    switch (opcion) {
      case "1":
        obtenerListaPendientes().then((pendientes) =>
          imprimirPendientesPorIDs(pendientes)
        );
        break;
      case "2":
        obtenerListaPendientes().then((pendientes) =>
          imprimirPendientesPorIDsYTitulos(pendientes)
        );
        break;
      case "3":
        obtenerListaPendientes().then((pendientes) =>
          imprimirPendientesNoResueltosPorIDsYTitulos(pendientes)
        );
        break;
      case "4":
        obtenerListaPendientes().then((pendientes) =>
          imprimirPendientesResueltosPorIDsYTitulos(pendientes)
        );
        break;
      case "5":
        obtenerListaPendientes().then((pendientes) =>
          imprimirPendientesPorIDsYUserID(pendientes)
        );
        break;
      case "6":
        obtenerListaPendientes().then((pendientes) =>
          imprimirPendientesResueltosPorIDsYUserID(pendientes)
        );
        break;
      case "7":
        obtenerListaPendientes().then((pendientes) =>
          imprimirPendientesNoResueltosPorIDsYUserID(pendientes)
        );
        break;
      case "8":
        console.log("Saliendo de la aplicación...");
        rl.close();
        break;
      default:
        console.log(
          "Opción no válida. Por favor, seleccione una opción del 1 al 8."
        );
        mostrarMenu();
        break;
    }
  });
}

mostrarMenu();
