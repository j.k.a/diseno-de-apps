import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack"; // Importa createNativeStackNavigator desde la biblioteca de navegación
import { SafeAreaView, StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import Inicio from "./screens/Inicio";
import Detalles from "./screens/Detalles";
import ResueltosIDyTitulo from "./screens/ResueltosIDyTitulo";
import ResueltosIDyUser from "./screens/ResueltosIDyUser";
import SinResolvIDyTitulo from "./screens/SinResolvIDyTitulo";
import SinResolvIDyUser from "./screens/SinResolvIDyUser";
import TodosID from "./screens/TodosID";
import TodosIDyTitulo from "./screens/TodosIDyTitulo";
import TodosIDyUser from "./screens/TodosIDyUser";

const Stack = createNativeStackNavigator(); // Mueve la declaración de Stack después de importar createNativeStackNavigator

const App = () => {
  const [hideSplashScreen, setHideSplashScreen] = React.useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        {hideSplashScreen ? (
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name="Inicio" component={Inicio} />
            <Stack.Screen name="Detalles" component={Detalles} />
            <Stack.Screen
              name="ResueltosIDyTitulo"
              component={ResueltosIDyTitulo}
            />
            <Stack.Screen
              name="ResueltosIDyUser"
              component={ResueltosIDyUser}
            />
            <Stack.Screen
              name="SinResolvIDyTitulo"
              component={SinResolvIDyTitulo}
            />
            <Stack.Screen
              name="SinResolvIDyUser"
              component={SinResolvIDyUser}
            />
            <Stack.Screen name="TodosID" component={TodosID} />
            <Stack.Screen name="TodosIDyTitulo" component={TodosIDyTitulo} />
            <Stack.Screen name="TodosIDyUser" component={TodosIDyUser} />
          </Stack.Navigator>
        ) : null}
      </NavigationContainer>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 24, // Ajusta este valor según sea necesario
  },
});

export default App;
