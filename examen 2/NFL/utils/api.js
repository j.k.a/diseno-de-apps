const obtenerListaPendientes = async () => {
  try {
    const response = await fetch("https://jsonplaceholder.typicode.com/todos");
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error("Error al obtener la lista de pendientes:", error);
  }
};

export { obtenerListaPendientes };
