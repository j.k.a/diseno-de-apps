import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
} from "react-native";
import { obtenerListaPendientes } from "../utils/api";
import { DataTable } from "react-native-paper";
import { Color, Padding, FontSize } from "../GlobalStyles";
import { useNavigation } from "@react-navigation/native";

const TodosIDyUser = () => {
  const [pendientes, setPendientes] = useState([]);
  const navigation = useNavigation();

  const irAtras = () => {
    navigation.goBack();
  };
  useEffect(() => {
    obtenerPendientes();
  }, []);

  const obtenerPendientes = async () => {
    try {
      const data = await obtenerListaPendientes();
      setPendientes(data);
    } catch (error) {
      console.error("Error al obtener la lista de pendientes:", error);
    }
  };

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <View style={{ flex: 1, padding: 16 }}>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Todos ID y User</Text>
        </View>
        <DataTable>
          <DataTable.Header>
            <DataTable.Title>ID</DataTable.Title>
            <DataTable.Title>UserID</DataTable.Title>
          </DataTable.Header>

          {pendientes.map((pendiente) => (
            <DataTable.Row key={pendiente.id}>
              <DataTable.Cell>{pendiente.id.toString()}</DataTable.Cell>
              <DataTable.Cell>{pendiente.userId}</DataTable.Cell>
            </DataTable.Row>
          ))}
        </DataTable>
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  topBar: {
    flexDirection: "row",
    alignItems: "center",
    padding: Padding.p_5xs,
    backgroundColor: Color.colorWhite,
    elevation: 6,
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 1,
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    color: Color.colorBlack,
    textAlign: "left",
    alignSelf: "stretch",
  },
});

export default TodosIDyUser;
