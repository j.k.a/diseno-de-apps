import React from "react";
import { Text, View, Pressable, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontSize, Color, Padding } from "../GlobalStyles";
import Container from "../components/Container";

const Inicio = () => {
  const navigation = useNavigation();

  const ResueltosIDyTitulo = () => {
    navigation.navigate("ResueltosIDyTitulo");
  };

  const ResueltosIDyUser = () => {
    navigation.navigate("ResueltosIDyUser");
  };

  const SinResolvIDyTitulo = () => {
    navigation.navigate("SinResolvIDyTitulo");
  };

  const SinResolvIDyUser = () => {
    navigation.navigate("SinResolvIDyUser");
  };

  const TodosID = () => {
    navigation.navigate("TodosID");
  };

  const TodosIDyTitulo = () => {
    navigation.navigate("TodosIDyTitulo");
  };

  const TodosIDyUser = () => {
    navigation.navigate("TodosIDyUser");
  };

  return (
    <View style={styles.container}>
      <View style={styles.topBar}>
        <Text style={styles.title}>Inicio</Text>
      </View>
      <Pressable onPress={TodosID} style={styles.listSpaceBlock}>
        <Container
          menuOptionText="Lista de todos los pendientes (solo IDs)"
          propAlignSelf="stretch"
          propWidth="unset"
          propColor="#000"
        />
      </Pressable>
      <Pressable onPress={TodosIDyTitulo} style={styles.listSpaceBlock}>
        <Container
          menuOptionText="Lista de todos los pendientes (IDs y Titles)"
          propAlignSelf="stretch"
          propWidth="unset"
          propColor="#000"
        />
      </Pressable>
      <Pressable onPress={SinResolvIDyTitulo} style={styles.listSpaceBlock}>
        <Container
          menuOptionText="Lista de todos los pendientes sin resolver (ID y Title)"
          propAlignSelf="stretch"
          propWidth="unset"
          propColor="#000"
        />
      </Pressable>
      <Pressable onPress={ResueltosIDyTitulo} style={styles.listSpaceBlock}>
        <Container
          menuOptionText="Lista de todos los pendientes resueltos (ID y Title)"
          propAlignSelf="stretch"
          propWidth="unset"
          propColor="#000"
        />
      </Pressable>
      <Pressable onPress={TodosIDyUser} style={styles.listSpaceBlock}>
        <Container
          menuOptionText="Lista de todos los pendientes (IDs y userID)"
          propAlignSelf="stretch"
          propWidth="unset"
          propColor="#000"
        />
      </Pressable>
      <Pressable onPress={ResueltosIDyUser} style={styles.listSpaceBlock}>
        <Container
          menuOptionText="Lista de todos los pendientes resueltos (ID y userID)"
          propAlignSelf="stretch"
          propWidth="unset"
          propColor="#000"
        />
      </Pressable>
      <Pressable onPress={SinResolvIDyUser} style={styles.listSpaceBlock}>
        <Container
          menuOptionText="Lista de todos los pendientes sin resolver (ID y userID)"
          propAlignSelf="stretch"
          propWidth="unset"
          propColor="#000"
        />
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.colorWhite,
    padding: Padding.p_xs,
  },
  topBar: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: Padding.p_5xs,
    backgroundColor: Color.colorWhite,
    elevation: 6,
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 1,
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    color: Color.colorBlack,
    textAlign: "left",
    flex: 1,
  },
  listSpaceBlock: {
    marginTop: 12,
  },
  icLeftIcon: {
    width: 24,
    height: 24,
    marginRight: 8,
  },
});

export default Inicio;
